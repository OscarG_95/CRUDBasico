<%-- 
    Document   : validacionForm
    Created on : 08-mar-2017, 15:03:09
    Author     : Oscar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Validacion Form</title>
        <link href="<c:url value="/resources/Css/bootstrap-3.3.7-dist/css/bootstrap.min.css"/>" rel="stylesheet"/>
        <script src="<c:url value="/resources/Js/jquery-3.1.1.min.js"/>"></script>
        <script src="<c:url value="/resources/Css/bootstrap-3.3.7-dist/js/bootstrap.min.js"/>"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <h1>Ingrese sus Datos</h1>
                <form:form method="POST" commandName="Person">
                    <form:errors path="*" element="div" cssClass="alert alert-danger"/>
                    <p>
                        <form:label path="Nombre">Nombre</form:label>
                        <form:input path="Nombre" cssClass="form-control"/>
                    </p>
                    <p>
                        <form:label path="Correo">Correo</form:label>
                        <form:input path="Correo" cssClass="form-control"/>
                    </p>
                    <p>
                        <form:label path="Pais">Pais</form:label>
                        <form:select path="Pais" cssClass="form-control">
                            <form:option value="0">Seleccione...</form:option>
                            <form:options items="${paisLista}"/>
                        </form:select>
                    </p>
                    <hr/>
                    <button class="form-control">Enviar</button>
                </form:form>
            </div>
        </div>
    </body>
</html>

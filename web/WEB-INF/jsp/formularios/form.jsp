<%-- 
    Document   : form
    Created on : 08-mar-2017, 13:52:42
    Author     : Oscar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Hola Bebe</title>
    </head>
    <body>
        <h1>Formulario</h1>
        
        <form:form>
            <p>
                <form:label path="Nombre">Nombre</form:label>
                <form:input path="Nombre"/>
            </p>
            <p>
                <form:label path="Correo">Correo</form:label>
                <form:input path="Correo"/>
            </p>
            <p>
                <form:label path="Edad">Edad</form:label>
                <form:input path="Edad"/>
            </p>
            <hr/>
            <form:button>Enviar</form:button>
        </form:form>
    </body>
</html>

<%-- 
    Document   : exito
    Created on : 08-mar-2017, 14:43:06
    Author     : Oscar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Resultado de Formulario</title>
        <link href="<c:url value="/resources/Css/bootstrap-3.3.7-dist/css/bootstrap.min.css"/>" rel="stylesheet"/>
        <script src="<c:url value="/resources/Js/jquery-3.1.1.min.js"/>"></script>
        <script src="<c:url value="/resources/Css/bootstrap-3.3.7-dist/js/bootstrap.min.js"/>"></script>
    </head>
    <body>
        <h1>Resultado de Formulario</h1>
        <ul>Nombre = ${Nombre}</ul>
        <ul>Correo = ${Correo}</ul>
        <ul>Pais = ${Pais}</ul>
    </body>
</html>

<%-- 
    Document   : boostrap
    Created on : 08-mar-2017, 12:42:16
    Author     : Oscar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>JSP Page</title>
        <link href="<c:url value="/resources/Css/bootstrap-3.3.7-dist/css/bootstrap.min.css"/>" rel="stylesheet"/>
        <script src="<c:url value="/resources/Js/jquery-3.1.1.min.js"/>"></script>
        <script src="<c:url value="/resources/Css/bootstrap-3.3.7-dist/js/bootstrap.min.js"/>"></script>
    </head>
    <body>
        <h1>Hello World!</h1>
        <table class="table table-bordered">
            <thead>
                <th>ID</th>
                <th>Name</th>
            </thead>
            <tbody>
                <tr>
                    <td>
                        1
                    </td>
                    <td>
                        Oscar González
                    </td>
                </tr>
                <tr>
                    <td>
                        2
                    </td>
                    <td>
                        Fulano de Tal
                    </td>
                </tr>
            </tbody>
        </table>
    <hr/>
    <div class="thumbnail">
        <img src="<c:url value="/resources/Images/images.jpg"/>" />
    </div>
    <hr />
    <a href="<c:url value="/home.htm"/>">ir a home</a>
</body>
</html>

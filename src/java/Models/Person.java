/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Oscar
 */
public class Person {
    
    private int Id;
    private String Nombre;
    private String Correo;
    private int Pais;

    public Person() {
    }

    public Person(String Nombre, String Correo, int Pais) {
        this.Nombre = Nombre;
        this.Correo = Correo;
        this.Pais = Pais;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public int getPais() {
        return Pais;
    }

    public void setPais(int Pais) {
        this.Pais = Pais;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author Oscar
 */
public class validatePerson implements Validator {

    private static final String EMAIL_PATTERN = "^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$";

    private Pattern pattern;
    private Matcher matcher;

    @Override
    public boolean supports(Class<?> type) {
        return Person.class.isAssignableFrom(type);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Person persona = (Person) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "Nombre", "required.Nombre", "El campo Nombre es obligatorio perrita");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "Correo", "required.Correo", "El campo Correo es obligatorio perrita");
        
        if (!(persona.getCorreo() != null && persona.getCorreo().isEmpty())) {
            this.pattern = Pattern.compile(EMAIL_PATTERN);
            this.matcher = pattern.matcher(persona.getCorreo());
            if (!matcher.matches()) {
                errors.rejectValue("Correo", "Correo.incorrect", "El correo " + persona.getCorreo() + " ingresado no es valido");
            }
        }
        
        if(persona.getPais()==0){
            errors.rejectValue("Pais", "required.Pais","Seleccione un pais");
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Models.Conexion;
import Models.Person;
import Models.Persona;
import Models.validatePerson;
import Models.validateUser;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Oscar
 */
@Controller
public class HomeController {

    //PARA VALIDACION
    private validatePerson personaValidar;
    //---------------
    
    //PARA BD
    private JdbcTemplate jdbcTemplate;
    //--------------
    
    //-------CRUD---------
    private validateUser validarusuario;
    private JdbcTemplate jdbcTemplateUser;

    public HomeController() {
        this.personaValidar = new validatePerson();
        Conexion conn = new Conexion();
        this.jdbcTemplate = new JdbcTemplate(conn.conectar());
        //----------CRUD------------
        this.validarusuario = new validateUser();
        this.jdbcTemplateUser = new JdbcTemplate(conn.conectar());
    }

    @RequestMapping("home.htm")
    public ModelAndView home() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("home/home");
        return mav;
    }

    @RequestMapping("nosotros.htm")
    public ModelAndView nosotros() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("nosotros");
        return mav;
    }

    @RequestMapping("boostrap.htm")
    public ModelAndView boostrap() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("home/boostrap");
        return mav;
    }

    @RequestMapping(value = "form.htm", method = RequestMethod.GET)
    public ModelAndView form() {
        ModelAndView mav = new ModelAndView("formularios/form", "command", new Persona());
        return mav;
    }

    @RequestMapping(value = "form.htm", method = RequestMethod.POST)
    public String form1(Persona p, ModelMap model) {
        model.addAttribute("Nombre", p.getNombre());
        model.addAttribute("Correo", p.getCorreo());
        model.addAttribute("Edad", p.getEdad());
        return "exito";//aqui va la interfaz a la q quiero redirigir 
    }

    @RequestMapping(value = "validacionForm.htm", method = RequestMethod.GET)
    public ModelAndView validacionForm() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("formularios/validacionForm");
        mav.addObject("Person", new Person());
        return mav;
    }

    //Metodo para crear select
    @ModelAttribute("paisLista")
    public HashMap<String, String> listaPais() {
        HashMap<String, String> pais = new HashMap();
        pais.put("1", "Colombia");
        pais.put("2", "Argentica");
        pais.put("3", "Chile");
        pais.put("4", "Mexico");
        pais.put("5", "Brasil");
        pais.put("6", "Venezuela");
        return pais;
    }

    //Recibir y validar datos FORM
    @RequestMapping(value = "validacionForm.htm", method = RequestMethod.POST)
    public ModelAndView validacionForm(@ModelAttribute("Person") Person p, BindingResult result, SessionStatus status) {
        this.personaValidar.validate(p, result);

        if (result.hasErrors()) {
            //Datos ingresados erroneos y se vuelve al mismo formulario
            ModelAndView mav = new ModelAndView();
            mav.setViewName("formularios/validacionForm");
            mav.addObject("Person", p);
            return mav;
        } else {
            ModelAndView mav = new ModelAndView();
            mav.setViewName("exito");
            mav.addObject("Nombre", p.getNombre());
            mav.addObject("Correo", p.getCorreo());
            mav.addObject("Pais", p.getPais());
            return mav;
        }
    }

    @RequestMapping("parametros.htm")
    public ModelAndView parametros(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("Parametros/Parametros");
        String identificador = request.getParameter("id");
        String identificador2 = request.getParameter("id2");
        mav.addObject("id", identificador);
        mav.addObject("id2", identificador2);
        return mav;
    }
    
    
    //-----------------------------------------------------------------------------------------------
    //-------------Base de Datos------------------- 
    
    @RequestMapping("homeBD.htm")
    public ModelAndView homeBD(){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("BD/homeBD");
        String sql = "select * from profesionesentrenador order by id asc;";
        List response;
        response = this.jdbcTemplate.queryForList(sql);
        mav.addObject("Datos", response);
        return mav;
    } 
    //------------------CRUD-----------------------
    
    @RequestMapping("add.htm")
    public ModelAndView add(){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("BD/add");
        return mav;
    }
    
    @RequestMapping("jsp2.htm")
    public ModelAndView jsp2(){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("jsp2");
        return mav;
    }
    
}
